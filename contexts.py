## Build contexts for epics-base docker image
## Syntax is
##  TAG: { <list of jinja2 variables> }

CONTEXTS = {
  '2.0.0': { 'base_ver': '7.0.6',
             'asyn_ver': (4,44,2),
             'as_ver': 'R5-10-2',
             'calc_ver': 'R3-7-4',
             'modbus_ver': (3,2),
             'snmp_ver': (1,1,0,2),
             'stream_ver': (2,8,22),
             'devthmpledp_ver': 'v1.0.0',
             'iocstats_ver': 'cpu-temp',
             'ftdi_support': 'yes',
           },
  '2.0.0-sbc': { 'base_ver': '7.0.6',
                 'asyn_ver': (4,42),
                 'as_ver': 'R5-10-2',
                 'calc_ver': 'R3-7-4',
                 'modbus_ver': (3,2),
                 'snmp_ver': (1,1,0,2),
                 'stream_ver': (2,8,22),
                 'drvasyni2c_ver': 'R1-0-3b',
                 'devgpio_ver': 'R2-0-1',
                 'devthmpledp_ver': 'v1.0.0',
                 'iocstats_ver': 'cpu-temp',
                 'ftdi_support': 'yes',
              },
}

