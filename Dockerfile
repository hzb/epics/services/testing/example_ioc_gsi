FROM paluma.ruhr-uni-bochum.de/epics/base:7.0.6 AS builder
ENV DEBIAN_FRONTEND noninteractive


RUN apt-get update \
      && apt-get install -qqy --no-install-recommends \
        curl \
        libsnmp-dev unzip \
        libpcre3-dev \
        libsocketcan-dev \
        python3 \
        libusb-dev libftdi1-dev \
      && apt-get clean && rm -rf /var/lib/apt/lists/* && rm -rf /var/cache/apt \
      && . /etc/profile.d/epics.sh \
      && mkdir -p ${EPICS_DIR}/modules \
      && useradd -d /config -g users -G dialout -s /bin/bash epics \
      && echo "SUPPORT = ${EPICS_DIR}/modules" > ${EPICS_DIR}/modules/RELEASE.local \
      && echo "undefine IPAC" >> ${EPICS_DIR}/modules/RELEASE.local \
      && echo "undefine PCRE" >> ${EPICS_DIR}/modules/RELEASE.local \
      && echo "undefine SNCSEQ" >> ${EPICS_DIR}/modules/RELEASE.local \
      && echo "undefine SSCAN" >> ${EPICS_DIR}/modules/RELEASE.local \
      && echo "EPICS_BASE = ${EPICS_DIR}/base" >> ${EPICS_DIR}/modules/RELEASE.local \
      && ln -s ${EPICS_DIR}/modules/RELEASE.local ${EPICS_SRC}/RELEASE.local \
      && echo "epicsIoc_DBD += PVAServerRegister.dbd qsrv.dbd" > ${EPICS_SRC}/modules.dbd \
      && echo 'epicsIoc_LIBS += qsrv $(EPICS_BASE_PVA_CORE_LIBS)' > ${EPICS_SRC}/modules.lib \
      && git clone --branch cpu-temp --depth 1 https://github.com/ffeldbauer/iocStats.git ${EPICS_SRC}/iocstats \
      && cd ${EPICS_SRC}/iocstats \
      && echo "MAKE_TEST_IOC_APP = NO" > configure/RELEASE.local \
      && echo 'INSTALL_LOCATION = $(SUPPORT)/iocstats' > configure/CONFIG_SITE.local \
      && make -j \
      && echo "epicsIoc_DBD += devIocStats.dbd" >> ${EPICS_SRC}/modules.dbd \
      && echo "epicsIoc_LIBS += devIocStats" >> ${EPICS_SRC}/modules.lib \
      && echo 'DEVIOCSTATS = $(SUPPORT)/iocstats' >> ${EPICS_DIR}/modules/RELEASE.local \
      && git clone --branch R5-10-2 --depth 1 https://github.com/epics-modules/autosave.git ${EPICS_SRC}/autosave \
      && cd ${EPICS_SRC}/autosave \
      && echo 'INSTALL_LOCATION=$(SUPPORT)/autosave' >> configure/CONFIG_SITE \
      && make -j \
      && echo "epicsIoc_DBD += asSupport.dbd" >> ${EPICS_SRC}/modules.dbd \
      && echo "epicsIoc_LIBS += autosave" >> ${EPICS_SRC}/modules.lib \
      && echo 'AUTOSAVE = $(SUPPORT)/autosave' >> ${EPICS_DIR}/modules/RELEASE.local \
      && git clone --depth 1 https://docker-pull-token:glpat-FhTgoB2xS6g1hUUDY-w4@panda-repo.gsi.de/pandadcs/devthmpledpulser.git ${EPICS_SRC}/devThmpLedPulser \
      && cd ${EPICS_SRC}/devThmpLedPulser \
      && echo 'INSTALL_LOCATION = $(SUPPORT)/devThmpLedPulser' > configure/CONFIG_SITE.local \
      && make -j \
      && echo "epicsIoc_DBD += devThmp.dbd bptThmpDegC.dbd devLedPulser2.dbd" >> ${EPICS_SRC}/modules.dbd \
      && echo "epicsIoc_LIBS += devThmp devLedPulser2" >> ${EPICS_SRC}/modules.lib \
      && echo 'DEVTHMPLEDPULSER = $(SUPPORT)/devThmpLedPulser' >> ${EPICS_DIR}/modules/RELEASE.local \
      && cd ${EPICS_SRC} \
      && mkdir ${EPICS_SRC}/snmp \
      && cd ${EPICS_SRC}/snmp \
      && curl --insecure -L -O https://groups.nscl.msu.edu/controls/files/epics-snmp-1.1.0.2.zip \
      && unzip epics-snmp-1.1.0.2.zip \
      && echo '-include $(TOP)/../RELEASE.local' >> configure/RELEASE \
      && echo 'INSTALL_LOCATION_APP=$(SUPPORT)/snmp' > configure/RELEASE.Common.${EPICS_HOST_ARCH} \
      && make \
      && echo "epicsIoc_DBD += devSnmp.dbd" >> ${EPICS_SRC}/modules.dbd \
      && echo "epicsIoc_LIBS += devSnmp" >> ${EPICS_SRC}/modules.lib \
      && echo "USR_CLAGS += `net-snmp-config --cflags`" >> ${EPICS_SRC}/modules.lib \
      && echo "USR_LDFLAGS += `net-snmp-config --libs`" >> ${EPICS_SRC}/modules.lib \
      && echo "PROD_LDLIBS += `net-snmp-config --libs`" >> ${EPICS_SRC}/modules.lib \
      && echo 'SNMP = $(SUPPORT)/snmp' >> ${EPICS_DIR}/modules/RELEASE.local \
      && curl -o /tmp/wiener-mib.zip --insecure -L http://file.wiener-d.com/software/net-snmp/WIENER-CRATE-MIB-5704.zip \
      && unzip -p /tmp/wiener-mib.zip WIENER-CRATE-MIB-5704.txt > ${EPICS_DIR}/WIENER-CRATE-MIB.txt \
      && rm /tmp/wiener-mib.zip \
      && git clone --branch R3-7-4 --depth 1 https://github.com/epics-modules/calc.git ${EPICS_SRC}/calc \
      && cd ${EPICS_SRC}/calc \
      && echo 'INSTALL_LOCATION = $(SUPPORT)/calc' > configure/CONFIG_SITE.local \
      && make -j \
      && echo "epicsIoc_DBD += calc.dbd" >> ${EPICS_SRC}/modules.dbd \
      && echo "epicsIoc_LIBS += calc" >> ${EPICS_SRC}/modules.lib \
      && echo 'CALC = $(SUPPORT)/calc' >> ${EPICS_DIR}/modules/RELEASE.local \
      && git clone --branch R4-44-2 --depth 1 https://github.com/epics-modules/asyn.git ${EPICS_SRC}/asyn \
      && cd ${EPICS_SRC}/asyn \
      && echo 'INSTALL_LOCATION = $(SUPPORT)/asyn' > configure/CONFIG_SITE.local \
      && echo 'DRV_FTDI = YES' >> configure/CONFIG_SITE.local \
      && echo 'DRV_FTDI_USE_LIBFTDI1 = YES' >> configure/CONFIG_SITE.local \
      && echo "epicsIoc_DBD += drvAsynFTDIPort.dbd" >> ${EPICS_SRC}/modules.dbd \
      && make -j \
      && echo "epicsIoc_DBD += asyn.dbd drvAsynIPPort.dbd drvAsynSerialPort.dbd" >> ${EPICS_SRC}/modules.dbd \
      && ( echo "epicsIoc_LIBS += asyn" | cat - ${EPICS_SRC}/modules.lib > /tmp/foo && mv /tmp/foo ${EPICS_SRC}/modules.lib ) \
      && echo 'ASYN = $(SUPPORT)/asyn' >> ${EPICS_DIR}/modules/RELEASE.local \
      && git clone --branch R3-2 --depth 1 https://github.com/epics-modules/modbus.git ${EPICS_SRC}/modbus \
      && cd ${EPICS_SRC}/modbus \
      && echo 'INSTALL_LOCATION = $(SUPPORT)/modbus' > configure/CONFIG_SITE.local \
      && make -j \
      && echo "epicsIoc_DBD += modbusSupport.dbd" >> ${EPICS_SRC}/modules.dbd \
      && ( echo "epicsIoc_LIBS += modbus" | cat - ${EPICS_SRC}/modules.lib > /tmp/foo && mv /tmp/foo ${EPICS_SRC}/modules.lib ) \
      && echo 'MODBUS = $(SUPPORT)/modbus' >> ${EPICS_DIR}/modules/RELEASE.local \
      && git clone --branch 2.8.22 --depth 1 https://github.com/paulscherrerinstitute/StreamDevice.git ${EPICS_SRC}/stream \
      && cd ${EPICS_SRC}/stream \
      && echo 'INSTALL_LOCATION = $(SUPPORT)/stream' > configure/CONFIG_SITE.local \
      && echo "PCRE_LIB = $(find /usr/lib -name libpcre.so -printf "%h")" >> configure/CONFIG_SITE.local \
      && make -j \
      && echo "epicsIoc_DBD += stream.dbd" >> ${EPICS_SRC}/modules.dbd \
      && ( echo "epicsIoc_LIBS += stream" | cat - ${EPICS_SRC}/modules.lib > /tmp/foo && mv /tmp/foo ${EPICS_SRC}/modules.lib ) \
      && echo 'STREAM = $(SUPPORT)/stream' >> ${EPICS_DIR}/modules/RELEASE.local \
      && mkdir ${EPICS_SRC}/ioc \
      && git clone --branch v2.0.1 --depth 1 https://panda-repo.gsi.de/pandadcs/epics-files.git ${EPICS_DIR}/ioc \
      && cd ${EPICS_SRC}/ioc \
      && makeBaseApp.pl -u epics -t ioc epicsIoc \
      && echo 'INSTALL_LOCATION = ${EPICS_DIR}/ioc' > configure/CONFIG_SITE.local \
      && sed -i "s/\(epicsIoc_DBD\s*+=\)\s*base.dbd/\1 menuGlobal.dbd menuScan.dbd stdRecords.dbd filters.dbd links.dbd devSoft.dbd asSub.dbd dbCore.dbd rsrv.dbd/" epicsIocApp/src/Makefile \
      && sed -i "/^#epicsIoc_DBD\s*+=.*/ r ${EPICS_SRC}/modules.dbd" epicsIocApp/src/Makefile \
      && sed -i "/^#epicsIoc_LIBS\s*+=.*/ r ${EPICS_SRC}/modules.lib" epicsIocApp/src/Makefile \
      && make \
      && cd / \
      && rm -rf ${EPICS_SRC} \
      && find /epics -name "*.a" -delete

##################################################################################################
## Deploy IOC

FROM paluma.ruhr-uni-bochum.de/epics/base:7.0.6 AS ta_ioc

COPY --from=builder /epics /epics


RUN echo "deb http://deb.debian.org/debian $(grep CODENAME /etc/os-release | awk -F'=' '{ print $2 }') non-free" > /etc/apt/sources.list.d/nonfree.list \
      && apt-get update \
      && apt-get install -qqy --no-install-recommends libreadline8 \
        libsnmp40 snmp-mibs-downloader \
        libpcre3 \
        libsocketcan2 \
        libusb-dev libftdi1-dev \
      && apt-get clean && rm -rf /var/lib/apt/lists/* && rm -rf /var/cache/apt \
      && download-mibs \
      && mv /epics/WIENER-CRATE-MIB.txt /usr/share/snmp/mibs/WIENER-CRATE-MIB.txt \
      && useradd -d /config -g users -G dialout -s /bin/bash epics \
      && rm -f /etc/timezone /etc/localtime \
      && echo "Europe/Berlin" > /etc/timezone \
      && ln -snf /usr/share/zoneinfo/Europe/Berlin /etc/localtime \
      && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
USER epics
VOLUME ["/config"]
WORKDIR /config
CMD ["/bin/bash"]
