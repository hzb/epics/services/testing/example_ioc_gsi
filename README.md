# Image for EPICS IOC

Builds an EPICS IOC with various support libraries

This image uses [epics-base](base/Dockerfile.jinja2) as base image to be build

## Usage

Default run command (standalone):
```bash
docker run -dit -v <LOCAL_DIR>:/config --name <NAME_OF_CONTAINER> [OPTIONS] paluma.ruhr-uni-bochum.de/epics/ioc <IOC_STARTUP_SCRIPT>
```
OPTIONS can be:
- Connect to serial devices:
  `--device <DEVICENAME>[:<NAME_INSIDE_CONTAINER>]`
  Examples:
    - `--device /dev/ttyS0      ## add serial port0 to container`
    - `--device /dev/gpiochip0  ## add gpio interface to container (this is mandatory if build with devGpio support)`
- Connect to ethernet devices:
  *Works without any additional settings*
- Connect to CAN bus devices:
  `--network host`
- Add user inside container to a group:
  `--group-add $(getent group | grep <GROUPNAME> | cut -d ":" -f 3)`
- Access PVs from remote:
  - CA: `-p 5064-5065:5064-5065 -p 5064-5065:5064-5065/udp` (might be sufficient to publish CAS ports)
  - PVaccess: `--network host`
- Access remote PVs:
  - CA: `-e EPICS_CA_AUTO_ADDR_LIST=no -e EPICS_CA_ADDR_LIST=<LIST OF IP ADDRESSES>`
    UDP broadcasts are not forwarded by Docker's default network (bridge). Setting `EPICS_CA_ADDR_LIST` to the broadcast address (e.g. `192.168.0.255`) does not work
    *Alternatively use `--network host`*
  - PVaccess: `--network host`

To run commands within the IOC container use `docker attach <CONTAINER ID>` and you should see the EPICS IOC command prompt.
To detach from the container again use `CTRL+p CTRL+q`

Run multiple IOC on same host: see [compose file](test/ioc-test.yml)

## Build

The Dockerfile uses a multistage build with 2 stages (`builder`, `TA_ioc`).

```bash
jinja2-render <TAG>
# single architecture build:
docker build --pull -t epics-ioc:<TAG> .
# for multiarch build:
docker buildx build --pull -t <REGISTRY>/epics/ioc:<TAG> --platform=<PLATFORM> --push .
```

`<TAG>` refers to a build set in (contexts.py)[ioc/contexts.py]

Docker-buildx requires a docker registry (either a local one or docker hub) as the images are build within a docker container and not stored in your local image registry.
`<PLATFORM>` can be e.g. `linux/amd64,linux/arm/v7`

On Debian, when installing docker from the official docker repository, docker buildx is installed as
`/usr/libexec/docker/cli-plugins/docker-buildx`

## Jinja varaibles

| Variable        | Description                                             |   Default value                  |
|-----------------|---------------------------------------------------------|:--------------------------------:|
| registry        | Registry containing EPICS base image                    | paluma.ruhr-uni-bochum.de/epics/ |
| base_ver        | Version of EPICS base image (mandatory)                 |   latest                         |
| epics_modules   | Install path for module libs                            |   modules                        |
| ioc_top         | Install path for IOC                                    |   ioc                            |
| myIOC           | Name of ioc                                             |   epicsIoc                       |
| execute_ver     | Version of devExecute module                            |                                  |
| iocstats_ver    | Version of devIocStats module as Py Tuple               |                                  |
| as_ver          | Version of autosave module                              |                                  |
| devgpio_ver     | Version of devGpio module                               |                                  |
| devthmpledp_ver | Version of devThmp/devLepPulser modules                 |                                  |
| snmp_ver        | Version of devSNMP module as Py tuple                   |                                  |
| wiener_mib_ver  | Version of Wiener MIB file for SNMP                     |   5704                           |
| calc_ver        | Version of calc module                                  |                                  |
| asyn_ver        | Version of asyn module as Python tuple                  |                                  |
| ftdi_support    | Compile asyn with ftdi support                          |                                  |
| drvasyncan_ver  | Version of drvAsynCan module                            |                                  |
| drvasyni2c_ver  | Version of drvAsynI2C module                            |                                  |
| modbus_ver      | Version of modbus module as Py tuple                    |                                  |
| stream_ver      | Version of streamDevice module                          |                                  |
| dbfile_ver      | Version of DB and proto file repo                       |   v2.0.1                         |
| static_link     | Link IOC statically (only EPICS libs)                   |                                  |
| timezone        | Used timezone                                           |   Europe/Berlin                  |
| epics_dir       | TOP path for epics installation (must match var in base |   /epics                         |

If you want to use a local epics-base image as base for your build, define variable `registry` with an empty value in (contexts.py)[ioc/contexts.py].
*This is the only variable with default value where an empty value is supported*


## Predefined tags

| Variable        |  2.0.0    | 2.0.0-sbc |
|-----------------|:---------:|:---------:|
| registry        | *default* | *default* |
| base_ver        | 7.0.6     | 7.0.6     |
| epics_modules   | *default* | *default* |
| ioc_top         | *default* | *default* |
| myIOC           | *default* | *default* |
| execute_ver     |           |           |
| iocstats_ver    | cpu-temp  | cpu-temp  |
| as_ver          | R5-10-2   | R5-10-2   |
| devgpio_ver     |           | R2-0-1    |
| devthmpledp_ver | v1.0.0    | v1.0.0    |
| snmp_ver        | (1,1,0,2) | (1,1,0,2) |
| wiener_mib_ver  | *default* | *default* |
| calc_ver        | R3-7-4    | R3-7-4    |
| asyn_ver        | (4,42)    | (4,42)    |
| ftdi_support    | yes       | yes       |
| drvasyncan_ver  |           |           |
| drvasyni2c_ver  |           | R1-0-3b   |
| modbus_ver      | (3,2)     | (3,2)     |
| stream_ver      | (2,8,22)  | (2,8,22)  |
| dbfile_ver      | *default* | *default* |
| static_link     |           |           |
| timezone        | *default* | *default* |
| epics_dir       | *default* | *default* |

